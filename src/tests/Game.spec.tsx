import { render, fireEvent, RenderResult } from "@testing-library/react";
import Game from "../screens/game/Game";

let renderResult: RenderResult;

describe("Test Suite", () => {
  beforeEach(() => {
    renderResult = render(<Game />)
  });

  it("renders game headings", () => {
    expect(renderResult.getByText("TIC-TAC-LIVEN")).toBeTruthy();
  });
  
  it("renders board and check for step counter update", () => {
  
    // Expect "Current step: 0" to be found
    expect(renderResult.getByText("Current step: 0")).toBeTruthy();
  
    const square0 = renderResult.getByTestId(`square-0`);
    fireEvent.click(square0);
  
    // Expect "Current step: 1" to be found
    expect(renderResult.getByText("Current step: 1")).toBeTruthy();
  });
  
  it("renders board and check for step counter update", () => {
  
    // Expect "Current step: 0" to be found
    expect(renderResult.getByText("Current step: 0")).toBeTruthy();
  
    const square0 = renderResult.getByTestId(`square-0`);
    fireEvent.click(square0);
  
    // Expect "Current step: 1" to be found
    expect(renderResult.getByText("Current step: 1")).toBeTruthy();
  });

  it("should be able player to X's win!", () => {
    ['square-0', 'square-1', 'square-3', 'square-2', 'square-6'].forEach((square) => {
      const squareObj = renderResult.getByTestId(square);
      fireEvent.click(squareObj);
    });

    expect(renderResult.getByText("Winner: X")).toBeTruthy();
  });

  it("should be able to player O's win!", () => {
    ['square-0', 'square-3', 'square-7', 'square-4', 'square-1' , 'square-5'].forEach((square) => {
      const squareObj = renderResult.getByTestId(square);
      fireEvent.click(squareObj);
    });

    expect(renderResult.getByText("Winner: O")).toBeTruthy();
  });

  it("should be able to ensure that the state of the board does not change when double-clicking on the same block", () => {
    ['square-0', 'square-0'].forEach((square) => {
      const squareObj = renderResult.getByTestId(square);
      fireEvent.click(squareObj);
    });

    expect(renderResult.getByText("Current step: 1")).toBeTruthy();
  });

  it("should be able to reset game", () => {
    ['square-0', 'square-1'].forEach((square) => {
      const squareObj = renderResult.getByTestId(square);
      fireEvent.click(squareObj);
    });

    const buttonReset = renderResult.getByTestId('button-restart');

    fireEvent.click(buttonReset);

    expect(renderResult.getByText("Current step: 0")).toBeTruthy();
  });

});


