/**
 * Obs: O controle de estado principal da aplicação deve ser mantido neste hook
 */

import { useState } from "react";

type Player = 'X' | 'O';

const initialBoard = Array(9).fill(null);

const useGameState = () => {
  const [stepNumber, setStepNumber] = useState(0);
  const [nextPlayer, setNextPlayer] = useState<Player>('X');
  const [currentBoard, setCurrentBoard] = useState(initialBoard);
  const [initialPlayer, setInitialPlayer] = useState<Player>('X');

  const restartGame = (): void => {
    setCurrentBoard(initialBoard);
    setStepNumber(0);
    initialPlayer === 'X' ? setInitialPlayer('O') : setInitialPlayer('X');
    setNextPlayer(initialPlayer);
  }

  const computeMove = (currentPlayer: Player, squareId: any): void => {

    currentPlayer === 'X' ? setNextPlayer('O') : setNextPlayer('X');

    const newBoard = [... currentBoard];
    newBoard[squareId] = currentPlayer;

    setCurrentBoard(newBoard);

    setStepNumber((currentStepNumber) => currentStepNumber + 1);
  }

  return {
    nextPlayer,
    stepNumber,
    currentBoard,
    computeMove,
    restartGame
  }
}

export default useGameState;
