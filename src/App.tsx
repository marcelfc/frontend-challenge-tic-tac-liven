import "./styles.css";
import Game from "./screens/game/Game";

export default function App() {
  return <Game />;
}
