interface SquareProps {
    id: number;
    value: string;
    onClick: () => {};
}

const Square: React.FC<SquareProps> = (props: SquareProps) => {

    const { id, value, onClick } : any = props;
    const renderIcon = (value: string) => {
      if(!value) {
        return null;
      } else {
        return value === 'X' ? "❌" : "⭕"
      }
    }
    return (
      <button data-testid={`square-${id}`} className="square" onClick={onClick}>
        {renderIcon(value)}
      </button>
    );
}

export default Square;