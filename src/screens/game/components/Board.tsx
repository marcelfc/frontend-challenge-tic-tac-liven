import Square from "./Square";

interface BoardProps {
    squares: Array<string>;
    onSquareClick: (arg: number) => any;
}

const Board: React.FC<BoardProps> = (props: BoardProps) => {

    const { squares, onSquareClick } = props;
    const renderSquare = (squareId: number) => {
        return (
          <Square
            id={squareId}
            value={squares[squareId]}
            onClick={() => onSquareClick(squareId)}
          />
        );
      };
    
      return (
        <div>
          <div className="board-row">
            {renderSquare(0)}
            {renderSquare(1)}
            {renderSquare(2)}
          </div>
          <div className="board-row">
            {renderSquare(3)}
            {renderSquare(4)}
            {renderSquare(5)}
          </div>
          <div className="board-row">
            {renderSquare(6)}
            {renderSquare(7)}
            {renderSquare(8)}
          </div>
        </div>
      );
}

export default Board;