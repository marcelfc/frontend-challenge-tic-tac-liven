import useGameState from "../../context/useGameState";
import Board from "./components/Board";
import { calculateWinner } from "./functions/utils";

const Game: React.FC = () => {
  const {
    currentBoard,
    stepNumber,
    nextPlayer,
    computeMove,
    restartGame
  } = useGameState();

  const handleSquareClick = (squareId: number) => {
    if (calculateWinner(currentBoard) || currentBoard[squareId]) {
      // Game over or square already handled
      return;
    }
    computeMove(nextPlayer, squareId);
  };

  const renderStatusMessage = () => {
    const winner = calculateWinner(currentBoard);
    if (winner) {
      return "Winner: " + winner;
    } else if (stepNumber === 9) {
      return "Draw: Game over";
    } else {
      return "Next player: " + (nextPlayer === 'X' ? "❌" : "⭕");
    }
  };


  return (
    <>
      <h1>
        TIC-TAC-LIVEN{" "}
        <span role="img" aria-label="rocket">
          🚀
        </span>
      </h1>
      <div className="game">
        <div className="game-board">
          <Board squares={currentBoard} onSquareClick={handleSquareClick} />
        </div>
        <div className="game-info">
          <div>Current step: {stepNumber}</div>
          <div>{renderStatusMessage()}</div>
          <div>
            <button type="button" data-testid="button-restart" className="btn-restart" onClick={restartGame}>Restart!</button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Game;
